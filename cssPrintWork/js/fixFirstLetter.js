
class fixFirstLetter extends Paged.Handler {
    constructor(chunker, polisher, caller) {
        super(chunker, polisher, caller);
    }

    beforeParsed(content) {
        content.querySelectorAll("header + p, header + figure + p").forEach(p => {
            p.classList.add('first-paragraph');
            let firstLetter = p.innerHTML[0];


            const regFirstLetter = new RegExp(/[A-Z]/);




            if (regFirstLetter.test(firstLetter)) {

                p.innerHTML = p.innerHTML.slice(1);


                //remove the first letter and add the find the end of the sentence.
                //set the beginning of the first sentence (and remove it first letter)
                p.innerHTML = `<span class=" introduction_text  first-sentence">${p.innerHTML}`

                // find the first sentence that can end with . ? and !
                const regFirstSentenceEnd = Object.assign({}, p.innerHTML.match(/\!|\?|\.|\…/));

                p.innerHTML = p.innerHTML.slice(0, regFirstSentenceEnd.index + 1) + '</span>' + p.innerHTML.slice(regFirstSentenceEnd.index + 1);


                // add the first letter before the element to float it
                p.insertAdjacentHTML('afterbegin', `<span class="first-letter">${firstLetter}</span>`)


            }
        })

    }
}

Paged.registerHandlers(fixFirstLetter);


insert = function (string, insert, position) {
    if (typeof (position) == 'undefined') {
        position = 0;
    }
    if (typeof (insertString) == 'undefined') {
        insert = '';
    }
    return string.slice(0, pos) + insert + string.slice(position)
}
