class CSStoClass extends Paged.Handler {
  constructor(chunker, polisher, caller) {
    super(chunker, polisher, caller);
    this.floatSameTop;
    this.floatSameColTop;
    this.floatSameColBottom;
    this.floatSameBottom;
    this.floatNextTop;
    this.floatNextBottom;
    this.floatNextColTop;
    this.floatNextColBottom;
    this.floatFullPage;
  }
  onDeclaration(declaration, dItem, dList, rule) {
    if (declaration.property == "--page-float") {
      if (declaration.value.value.includes("same-top")) {
        let sel = csstree.generate(rule.ruleNode.prelude);
        sel = sel.replace('[data-id="', "#");
        sel = sel.replace('"]', "");
        this.floatSameTop = sel.split(",");
      } else if (declaration.value.value.includes("same-column-top")) {
        let sel = csstree.generate(rule.ruleNode.prelude);
        sel = sel.replace('[data-id="', "#");
        sel = sel.replace('"]', "");
        this.floatSameColTop = sel.split(",");
      } else if (declaration.value.value.includes("same-column-bottom")) {
        let sel = csstree.generate(rule.ruleNode.prelude);
        sel = sel.replace('[data-id="', "#");
        sel = sel.replace('"]', "");
        this.floatSameColBottom = sel.split(",");
      } else if (declaration.value.value.includes("same-bottom")) {
        let sel = csstree.generate(rule.ruleNode.prelude);
        sel = sel.replace('[data-id="', "#");
        sel = sel.replace('"]', "");
        this.floatSameBottom = sel.split(",");
      } else if (declaration.value.value.includes("next-top")) {
        let sel = csstree.generate(rule.ruleNode.prelude);
        sel = sel.replace('[data-id="', "#");
        sel = sel.replace('"]', "");
        this.floatNextTop = sel.split(",");
      } else if (declaration.value.value.includes("next-bottom")) {
        let sel = csstree.generate(rule.ruleNode.prelude);
        sel = sel.replace('[data-id="', "#");
        sel = sel.replace('"]', "");
        this.floatNextBottom = sel.split(",");
      } else if (declaration.value.value.includes("next-column-top")) {
        let sel = csstree.generate(rule.ruleNode.prelude);
        sel = sel.replace('[data-id="', "#");
        sel = sel.replace('"]', "");
        this.floatNextColTop = sel.split(",");
      } else if (declaration.value.value.includes("next-column-bottom")) {
        let sel = csstree.generate(rule.ruleNode.prelude);
        sel = sel.replace('[data-id="', "#");
        sel = sel.replace('"]', "");
        this.floatNextColBottom = sel.split(",");
      } else if (declaration.value.value.includes("full-page")) {
        let sel = csstree.generate(rule.ruleNode.prelude);
        sel = sel.replace('[data-id="', "#");
        sel = sel.replace('"]', "");
        this.floatFullPage = sel.split(",");
      }
    }
  }
  afterParsed(content) {
    if (this.floatNextBottom) {
      this.floatNextBottom.forEach((elNBlist) => {
        content.querySelectorAll(elNBlist).forEach((el) => {
          el.classList.add("page-float-next-bottom");
        });
      });
    }
    if (this.floatNextTop) {
      this.floatNextTop.forEach((elNBlist) => {
        content.querySelectorAll(elNBlist).forEach((el) => {
          el.classList.add("page-float-next-top");
        });
      });
    }
    if (this.floatSameColTop) {
      this.floatSameColTop.forEach((elNBlist) => {
        content.querySelectorAll(elNBlist).forEach((el) => {
          el.classList.add("page-float-col-top");
        });
      });
    }
    if (this.floatSameColBottom) {
      this.floatSameColBottom.forEach((elNBlist) => {
        content.querySelectorAll(elNBlist).forEach((el) => {
          el.classList.add("page-float-col-bottom");
        });
      });
    }
    if (this.floatSameTop) {
      this.floatSameTop.forEach((elNBlist) => {
        content.querySelectorAll(elNBlist).forEach((el) => {
          el.classList.add("imgToTop");
        });
      });
    }
    if (this.floatSameBottom) {
      this.floatSameBottom.forEach((elNBlist) => {
        content.querySelectorAll(elNBlist).forEach((el) => {
          el.classList.add("imgToBottom");
        });
      });
    }
    if (this.floatFullPage) {
      this.floatFullPage.forEach((elNBlist) => {
        content.querySelectorAll(elNBlist).forEach((el) => {
          el.classList.add("imgFullPage");
        });
      });
    }
  }
}

Paged.registerHandlers(CSStoClass);
