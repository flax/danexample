class columnMaker extends Paged.Handler {
    constructor(chunker, polisher, caller) {
        super(chunker, polisher, caller);
    }

    beforeParsed(content) {
        for (let section of content.querySelectorAll('section.introduction, section.chapter')) {
            section.innerHTML = `<div class="cols">${section.innerHTML}</div>`
        }

        }


}

Paged.registerHandlers(columnMaker);