class baselineMoving extends Paged.Handler {
    constructor(chunker, polisher, caller) {
        super(chunker, polisher, caller);
    }

    renderNode(node, sourceNode) {
        if (
            node.nodeType == 1 &&
            node.tagName == "P" &&
            node.previousElementSibling &&
            node.previousElementSibling.style.display != "none"
        ) {
            if (
                node.previousElementSibling.tagName === "H2" ||
                node.previousElementSibling.tagName === "H3" ||
                node.previousElementSibling.tagName === "OL" ||
                node.previousElementSibling.tagName === "UL" ||
                node.previousElementSibling.classList.contains("wrapper") ||
                node.previousElementSibling.tagName === "table" ||
                node.previousElementSibling.tagName === "figure"

            ) {
                startBaseline(node, 18);
            }
        }
    }
}

Paged.registerHandlers(baselineMoving);


function startBaseline(element, baseline) {

    if (element) {
        const elementOffset = element.offsetTop;
        const elementline = Math.floor(elementOffset / baseline);
        let pusher = document.createElement('div');
        pusher.classList.add("pusher");

        if (elementline != baseline) {
            const nextPline = (elementline + 1) * baseline;

            if (!(nextPline - elementOffset == baseline)) {
                pusher.style.height = `${nextPline - elementOffset}px`
                pusher.style.background = "red";
                pusher.style.columnSpan = "all";

                element.insertAdjacentElement("beforebegin", pusher);
                // element.style.paddingTop = `${nextPline - elementOffset}px`;
            }
        }
    }
}



