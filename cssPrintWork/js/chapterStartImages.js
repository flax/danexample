
class chapterStart extends Paged.Handler {
    constructor(chunker, polisher, caller) {
        super(chunker, polisher, caller);
    }

    beforeParsed(content) {
        content.querySelectorAll('section').forEach((section, sectionIndex) => {
            section.dataset.wrapper = sectionIndex;
            section.querySelectorAll('img').forEach(img => {
                img.classList.add(`imgStart-${sectionIndex}`)
            });

        })
    }

    afterRendered(pages) {

        for (let startPage of document.querySelectorAll('.pagedjs_prechapter_first_page, .pagedjs_chapter_first_page ')) {
            let sectionIntroWrap = document.createElement('div');
            sectionIntroWrap.classList = 'wrap-importImg'
            let sectionIntro = document.createElement('div');
            sectionIntro.classList = 'importImg';
            let sectionNumber = startPage.querySelector('section').dataset.wrapper;

            for (let img of document.querySelectorAll(`.imgStart-${sectionNumber}`)) {
                let clone = img.cloneNode(true);
                sectionIntro.insertAdjacentElement('afterbegin', clone);

            }
            sectionIntroWrap.insertAdjacentElement('afterbegin', sectionIntro);
            startPage.insertAdjacentElement('afterbegin', sectionIntroWrap);
        }
    }

}

Paged.registerHandlers(chapterStart);