function createBlock(content, elementTag, elementClassName, elements) {
    // create wrapper block

    let allFig = content.querySelectorAll(
        "figure, ul, ol, blockquote, .homeIcon"
    );
    allFig.forEach((fig) => {
        if (
            testClass(fig.previousElementSibling, elements) ||
            fig.previousElementSibling.classList.contains(
                `${elementClassName}-wrapper`
            )
        ) {
            fig.classList.add(`${elementClassName}-wrapper`);
        }
    });

    // move stiff in the previous box

    // set an anchor before each first element
    elements.forEach((ct) => {
        content.querySelectorAll(ct).forEach((elementWithClass, index) => {
            if (
                !testClass(elementWithClass.previousElementSibling, elements) &&
                !elementWithClass.previousElementSibling.classList.contains(
                    `${elementClassName}-wrapper`
                )
            ) {
                let block = document.createElement(elementTag);
                block.classList.add(elementClassName);
                block.classList.add("blockWrapper");
                elementWithClass.insertAdjacentElement("beforebegin", block);
            }
        });
    });

    // add Class To all image

    // Add Class To All Element
    elements.forEach((ct) => {
        content.querySelectorAll(ct).forEach((el) => {
            el.classList.add(`${elementClassName}-wrapper`);
            let next = el.nextElementSibling;
            if (
                next &&
                (next.tagName == "BLOCKQUOTE" ||
                    next.tagName == "FIGURE" ||
                    next.tagName == "OL" ||
                    next.tagName == "UL")
            ) {
                el.nextElementSibling.classList.add(`${elementClassName}-wrapper`);
            }
        });
    });

    content
        .querySelectorAll(`.${elementClassName}-wrapper`)
        .forEach((wrappedElement) => {
            if (wrappedElement.tagName === "figure") {
            }
            wrappedElement.previousElementSibling.insertAdjacentElement(
                "beforeend",
                wrappedElement
            );
        });
}

function testClass(el, array) {
    // remove # and . to the element lists
    let arrayUpdate = [];

    array.forEach((cn) => {
        cn = cn.replace("#", "");
        cn = cn.replace(".", "");
        arrayUpdate.push(cn);
    });
    // creates test case

    let classes = "\\b(" + arrayUpdate.join("|") + ")\\b",
        regex = new RegExp(classes, "i");
    if (regex.test(el.classList.value)) {
        return true;
    } else {
        return false;
    }
}



