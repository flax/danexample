
let classElemFloatSameTop = "page-float-col-top"; // ← class of floated elements on same page
// let classElemFloatSameBottom = "imgToBottom"; // ← class of floated elements bottom on same page


class imgToTopRight extends Paged.Handler {
    constructor(chunker, polisher, caller) {
        super(chunker, polisher, caller);
        this.floatBlock = document.createElement('div');
    }

    layoutNode(node) {
        if (node.nodeType == 1) {
            if (node.classList.contains(classElemFloatSameTop)) {
                let clone = node.cloneNode(true);
                this.floatBlock.classList.add("floatTop");
                this.floatBlock.insertAdjacentElement('afterbegin', clone);

                node.style.display = "none";

            }

        }
    }
    onOverflow(overflow, rendered, bounds) {
        console.log(this.floatBlock);
        if (rendered.querySelector('figure')) {
      
            rendered.querySelector(".cols")
            .insertAdjacentElement("beforebegin", this.floatBlock);


        }
    }
    afterPageLayout(pageElement, page, breakToken) {
        this.floatBlock.innerHTML = "";
    }
}

// Paged.registerHandlers(imgToTopRight);

