
class addID extends Paged.Handler {
	constructor(chunker, polisher, caller) {
		super(chunker, polisher, caller);
	}



	beforeParsed(content) {
		let tags = [
			"figure",
			"figcaption",
			"img",
			"ol",
			"ul",
			"li",
			"p",
			"img",
			"table",
			"h1",
			"h2",
			"h3",
			"div",
			"aside",
			"td",
			"tr",
			"th",
			"td"
		];
		content.querySelectorAll('section').forEach((section, sectionIndex) => {
		tags.forEach((tag) => {
				content.querySelectorAll(tag).forEach((el, index) => {
					if (!el.id) {
						el.id = `section-${sectionIndex}-el-${el.tagName.toLowerCase()}-${index}`;
					}
				});
			});
		})
	}
}

Paged.registerHandlers(addID);





class removeEmpties extends Paged.Handler {
	constructor(chunker, polisher, caller) {
		super(chunker, polisher, caller);
	}

	

	beforeParsed(content) {
		let tags = [
			
		];
		content.querySelectorAll('section').forEach((section, sectionIndex) => {
		tags.forEach((tag) => {
				content.querySelectorAll(tag).forEach((el, index) => {
					if (!el.id) {
						el.id = `section-${sectionIndex}-el-${el.tagName.toLowerCase()}-${index}`;
					}
				});
			});
		})
	}
}

Paged.registerHandlers(removeEmpties);