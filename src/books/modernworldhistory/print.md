---
title: Modern World History
authors: Dan allonso
folder: modernworldhistory
layout: print.njk
tags:
  - book
cover: /images/modernworldhistory/nasa-B7Q0Rv9jTkU-unsplash.jpg
---

<p class="author" data-ref="e3303907-d802-49fa-9f09-2b6b882cb0fc" data-previous-break-after="avoid">Dan Allosso,
    Bemidji State University</p>
<p class="author" data-ref="42cddd20-adfa-4924-bf2d-fd9d961f8db3"
    data-following="following-4d5a4583-b24c-4fb5-93ea-c5a4b87b4f48">Tom Williford, Southwest Minnesota State
    University</p>
<p class="paragraph" data-ref="30b5c0a9-98de-4659-aed5-753135cafcd0"
    data-following="following-4d5a4583-b24c-4fb5-93ea-c5a4b87b4f48"><span class="copyright"
        data-tags="[&quot;copyright&quot;]" data-ref="5ef8185e-0f4f-473d-ad94-0c39c5280f74" data-page="copyright">©
        2021</span></p>
<p class="paragraph" data-ref="dfce1042-bf63-4f07-81f8-05fe8ecf7f04" data-after-page="copyright"><span
        class="publisher" data-tags="[&quot;publisher&quot;]" data-ref="4a0ee89a-38be-41ad-9b0f-4bbb2bca11ee"><a
            href="https://mlpp.pressbooks.pub/" rel="" target="blank"
            data-ref="886680c0-bd4c-4346-912e-b02486aea8c9">Minnesota Libraries Publishing Project</a></span></p>
