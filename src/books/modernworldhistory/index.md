---
title: Modern World History
authors: Dan allonso
folder: modernworldhistory
class: cover
layout: bookindex.njk
# menu: true
tags:
  - book
coverImg: /images/modernworldhistory/nasa-B7Q0Rv9jTkU-unsplash.jpg
---

<p class="author">Dan Allosso, Bemidji State University</p>
<p class="author">Tom Williford, Southwest Minnesota State University</p>
<p class="paragraph"><span class="copyright">© 2021</span></p>
<p class="paragraph"><span class="publisher"><a href="https://mlpp.pressbooks.pub/">Minnesota Libraries Publishing Project</a></span></p>
