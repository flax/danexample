const elasticlunr = require("elasticlunr");


module.exports = function (value, folder) {
  // what fields we'd like our index to consist of
  var index = elasticlunr(function () {
    this.addField("title");
    this.addField("tags");
    this.addField("chapnum");
    this.addField("content");
    this.setRef("id");
  });

  // loop through each page and add it to the index
  value.forEach((item) => {
    if(item.data.folder == folder) {
    index.addDoc({
      id: item.url,
      content: item.templateContent,
      chapnum: item.data.order,
      title: item.data.title,
      tags: item.data.tags
      
    });
  }
  });

  return index.toJSON();
};
