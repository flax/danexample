// this will get the data from an instance of editoria using graphql

const fetch = require("node-fetch");

function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min)) + min;
}

// function to get books
async function getAllBooks() {
  // max number of records to fetch per query
  const recordsPerQuery = 100;

  // number of records to skip (start at 0)
  let recordsToSkip = 0;

  let makeNewQuery = true;

  let publishedBooksJSON = [];

  // make queries until makeNewQuery is set to false
  while (makeNewQuery) {
    try {
      // initiate fetch
      const data = await fetch("http://localhost:4000/graphql", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json",
        },
        body: JSON.stringify({
          query: `{
            getBookCollections {
                books {
                  id
                  title
                  divisions {
                      bookComponents {
                      title
                      content
                        prevBookComponent {
                            id
                            title
                        }
                        nextBookComponent {
                            id 
                            title
                        }
                      divisionType
                    }
                  }
                }
            }
          }`,
        }),
      });

      // store the JSON response when promise resolves
      const response = await data.json();
      console.log('response:', response.data.getBookCollections.books);
      
    //   response.data.getBookCollections.forEach(test => {
    //       console.log('test',test.books.title);
    //   } )

      // handle CMS errors
      if (response.errors) {
        let errors = response.errors;
        errors.map((error) => {
          console.log(error.message);
        });
        throw new Error("Houston... We have a CMS problem");
      }

    //   console.log('response', response.data.getBookCollections.books);
      // update blogpost array with the data from the JSON response
      publishedBooksJSON = publishedBooksJSON.concat(response.data.books);

      //   console.log(publishedBooksJSON);

      // prepare for next query
      recordsToSkip += recordsPerQuery;

      // stop querying if we are getting back less than the records we fetch per query
      if (response.data.books < recordsPerQuery) {
        makeNewQuery = false;
      }
    } catch (error) {
      throw new Error(error);
    }

    // format books objects
    const booksFormatted = publishedBooksJSON.map((item) => {
      return {
        // id: item.title,
        // sections: item.meta.articleSections,
        // articletype: item.meta.articleType,
        // statuts: item.status,
        // // history: {
        // // type: item.meta.history.type || '',
        // // date: item.meta.history.date  || '',
        // // },
        // title: item.meta.title,
        // published: item.published,
        // reviews: item.reviews,
        // body: content,
      };
    });

    // return formatted books
    return booksFormatted;
  }
}

// export for 11ty
// module.exports = getAllBooks;
