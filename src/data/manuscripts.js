// const fetch = require("node-fetch");

// function getRandomInt(min, max) {
//   return Math.floor(Math.random() * (max - min)) + min;
// }

// // function to get manuscripts
// async function getAllManuscripts() {
//   // max number of records to fetch per query
//   const recordsPerQuery = 100;

//   // number of records to skip (start at 0)
//   let recordsToSkip = 0;

//   let makeNewQuery = true;

//   let publishedManuscriptsJSON = [];

//   // make queries until makeNewQuery is set to false
//   while (makeNewQuery) {
//     try {
//       // initiate fetch
//       const data = await fetch("http://kotahidev.cloud68.co/graphql", {
//         method: "POST",
//         headers: {
//           "Content-Type": "application/json",
//           Accept: "application/json",
//         },
//         body: JSON.stringify({
//           query: `{
//             publishedManuscripts(sort: null, offset: 0, limit: 10000) {
//               totalCount
//               manuscripts {
//                 id
//                 reviews {
//                   id
//                   open
//                   recommendation
//                   reviewComment {
//                     content
//                   }
//                   created
//                   isDecision
//                   user {
//                     id
//                     username
//                   }
//                 }
//                 status
//                 files {
//                   id
//                   url
//                   filename
//                   fileType
//                   mimeType
//                 }
//                 meta {
//                   manuscriptId
//                   title
//                   articleSections
//                   articleType
//                   source
//                   history {
//                     type
//                     date
//                   }
//                 }
//                 published
//                 submission
//                 submitter {
//                   defaultIdentity {
//                     name
//                   }
//                 }
//               }
//             }
//           }`,
//         }),
//       });

//       // store the JSON response when promise resolves
//       const response = await data.json();

//       // handle CMS errors
//       if (response.errors) {
//         let errors = response.errors;
//         errors.map((error) => {
//           console.log(error.message);
//         });
//         throw new Error("Houston... We have a CMS problem");
//       }

//       // update blogpost array with the data from the JSON response
//       publishedManuscriptsJSON = publishedManuscriptsJSON.concat(
//         response.data.publishedManuscripts.manuscripts
//       );

//       // prepare for next query
//       recordsToSkip += recordsPerQuery;

//       // stop querying if we are getting back less than the records we fetch per query
//       if (response.data.publishedManuscripts < recordsPerQuery) {
//         makeNewQuery = false;
//       }
//     } catch (error) {
//       throw new Error(error);
//     }

//     // format manuscripts objects
//     const manuscriptsFormatted = publishedManuscriptsJSON.map((item) => {
//       // add ids to titles
//       let content = item.meta.source;
//       content = content.replace(
//         "<h1",
//         `<h1 id="title-${getRandomInt(1, 102389012802148)}"`
//       );
//       content = content.replace(
//         "<h2",
//         `<h2 id="title-${getRandomInt(1, 102389012802148)}"`
//       );
//       content = content.replace(
//         "<h3",
//         `<h3 id="title-${getRandomInt(1, 102389012802148)}"`
//       );
//       content = content.replace(
//         "<h4",
//         `<h4 id="title-${getRandomInt(1, 102389012802148)}"`
//       );
//       // add lazy loading
//       content = content.replace(
//         "<img",
//         `<img loading="lazy" decoding="async"`
//       );

//       return {
//         id: item.meta.id,
//         sections: item.meta.articleSections,
//         articletype: item.meta.articleType,
//         statuts: item.status,
//         // history: { 
//           // type: item.meta.history.type || '',
//           // date: item.meta.history.date  || '',
//         // },
//         title: item.meta.title,
//         published: item.published,
//         reviews:  item.reviews,
//         body: content,
//       };
//     });

//     // return formatted manuscripts
//     return manuscriptsFormatted;
//   }
// }

// // export for 11ty
// module.exports = getAllManuscripts;
