# webbook from anything

1. A reading interface 
    A. basic text styles (designer work)
    B. changing reading (reader choices)
    C. size changer (warning: browser zoom + )
    D. font changer (not much) 
    E. night mode / light mode
    F. offline mode
    G. Table of contents (per book)
    H. Table of contents (per chapter)
    I. element numbering
 
2. A blog interface
    → push stuff to a backend
    → pull stuff from a backend
    → theme for the blog
    → Wax to write stuff
    → reading interface from the blog a bit different

3. PRINT with PAGEDJS (custom size printing)

4. pull stuff from anywhere

5. 


BUILD

→ install eleventy

```sh
npm install -g @11ty/eleventy
```
→ or run eleventy 

```sh
npx @11ty/eleventy --serve
```


## Using nunjucks for templating




IDEAS:

- set/get reading position with cookies? (should we?) or use the browser save.
“last time you read this book, you were at this position. Do you want to resume your reading?” → netflix reading :D





search:

→ one searchindex json per book
→ one search pager per book
→ if something is found: 
    → show all the content, hide any element that doesn’t have a `mark` element in it.
    → each element is a previewable link to the element itself is the book.
    → each element needs to have an id to be usable later
